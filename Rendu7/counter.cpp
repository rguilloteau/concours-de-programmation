#include <iostream>
#include <cmath>
#include <stdio.h>

using namespace std;

 unsigned long l[54];

 unsigned long calcul( unsigned long k){
  if (k<=1)
    return k;
  int i = (int) floorl(log2l(k));
  unsigned long delta = (unsigned long) k-powl(2,i);
  return l[i]+calcul(delta)+delta+1;
}

int main() {
  l[0]=0;
  for (int i=1; i<55; i++)
    l[i]=2*l[i-1]+(unsigned long)(powl(2,i-1));

  unsigned long minimum,maximum;
  while(scanf("%lu %lu", &minimum, &maximum)!=EOF){
    cout << calcul(maximum)-calcul(minimum-1) <<endl;
  }
  return 0;
}
