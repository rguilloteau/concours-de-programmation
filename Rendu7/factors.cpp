#include <stdio.h>
#include <iostream>
#include <map>
#include <queue>
#include <cmath>
#include <cstdint>

using namespace std;

map<int,int> dict;
queue<int> input;
vector<uint64_t> premiers;
int exposants[63];
uint64_t fact[64];

void add_fk(int max, int index, int sum){
  if (max==0 || index==63){
    uint64_t k = 0;
    uint64_t tmp = 0;
    for (int i=0; i<index; i++){
      tmp+=exposants[i];
      k*=exposants[i]*premiers[i];
    }
    uint64_t n = fact[tmp];
    for (int i =0; i<index; i++){
      n/=fact[exposants[i]];
    }
    dict[n]=k;
  }
  else{
    for (int k=1; k<=max;k++){
      int tmp = sum + k;
      if (tmp<=63){
        exposants[index]=k;
        add_fk(k,index+1,sum+k);
      }
    }
  }
}


void eratosthene(uint64_t n){
  uint64_t nb_found = 0, i=1;
  while (nb_found < n){
      i++;
      bool premier = true;
      for (auto it : premiers){
        if (i%it==0){
          premier = false;
          break;
        }
        if(it>sqrt(i))
          break;
      }
      if (premier){
        nb_found++;
        premiers.push_back(i);
      }
    }
}

int main(){
  fact[0]=1;
  for (int i=0; i<63; i++){
    exposants[i]=0;
    fact[i+1]=fact[i]*(i+1);
  }

  uint64_t n, max_n = 0;
  while(scanf("%lu\n",&n)!=EOF){
    input.push(n);
    max_n = max(max_n, n);
  }

  eratosthene(max_n);

  add_fk(63,0,0);

  while(!input.empty()){
    cout << input.front()<<" "<<dict[input.front()]<<endl;
    input.pop();
  }


  return 0;
}
