#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <vector>

using namespace std;

class Chip{
  int power_min;

public:
  Chip(): power_min(-1){}

  void addFirstBattery(int b){
    this->power_min = b;
  }

  int addBattery(int batterie){return (this->power_min <= batterie);}

};

class Machine {

  Chip * chip1 = nullptr;
  Chip * chip2 = nullptr;


public:
  Machine(): chip1(new Chip()), chip2(new Chip()){}

public:
  void addFirstBatteryToBothChip(int batterie1, int batterie2){

    this->chip1->addFirstBattery(batterie1);
    this->chip2->addFirstBattery(batterie2);

  }

  bool addBattery(int batterie){
      return (this->chip1->addBattery(batterie) & this->chip2->addBattery(batterie));
  }

};



vector<int> batteries(1000000, 0);
vector<int> batteriesNotUsed(1000000, 0);
int n,k;
vector<Machine *> machines(500000, new Machine());

bool testD(int d){
    int next_machine = 0;
    int index_not_used =0;
    for (int i=0; i<2*n*k-1; i++){
      if ((batteries[i+1] - batteries[i] <= d) && (next_machine < n)) {
        machines[next_machine++]->addFirstBatteryToBothChip(batteries[i], batteries[i+1]);
        i++;
      }
      else{
        batteriesNotUsed[index_not_used++] = batteries[i];
      }

  }

  batteriesNotUsed[index_not_used++] = batteries[2*n*k-1];

  if (next_machine < n)
    return false;
  else{

    next_machine = 0;
    int compteur = 0;
    for (int i=0; i<index_not_used; i++){
      if(machines[next_machine]->addBattery(batteriesNotUsed[i])){
        compteur++;
        if (compteur==2*(k-1)){
          compteur=0;
          next_machine++;
        }
      }
      else{
        return false;
        }

    }

  }

    return true;
}

int main(){

  for (int i=0; i<500000; i++){
    machines[i] = new Machine();
  }

  while (scanf("%d %d\n", &n, &k)==2){


    for (int i=0; i<2*n*k; i++){
      scanf("%d", &batteries[i]);
    }
    sort(batteries.begin(), batteries.begin()+2*n*k);

    int d = batteries[1]-batteries[0];



    while(!testD(d))
      d++;

    printf("%d\n", d);

  }

}
