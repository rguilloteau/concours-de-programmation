#include <stdio.h>
#include <algorithm>
#include <math.h>
#include <limits>
#include <cmath>

using namespace std;

const long double INFINI  = std::numeric_limits<long double>::max();
const long double PI = 3.14159265358979323846264338327950288419716939937510L;

int permutation[6];
int cas =0;
int n;

class Ballon {
  int x, y, z;
  long double r=0;

public:
  Ballon(int x, int y, int z):x(x), y(y), z(z){}
  int getX(){return this->x;}
  int getY(){return this->y;}
  int getZ(){return this->z;}
  long double getR(){return this->r;}

  void setR(long double r){this->r = r;}

  long double getDistance(Ballon * b){
    long double res =sqrtf( powl(b->x - this->x, 2.0) + powl(b->y - this->y, 2.0) + powl(b->z - this->z, 2.0) );
    return res;
  }


  long double closestBalloon(int index, Ballon * ballons[]){
    long double min_r = INFINI;

    for (int i=0; i<index; i++){
      long double r = ballons[permutation[i]]->getR();
      if (r>0)
        min_r=min(min_r, this->getDistance(ballons[permutation[i]]) - r);
    }

    long double zero =0.0;
    return max(zero, min_r);
  }
  

};

Ballon * ballons[6];


class Box {
  int x1,x2,y1,y2,z1,z2;

public:
  Box (int x1,int x2,int y1,int y2,int z1,int z2):x1(x1),x2(x2),y1(y1),y2(y2),z1(z1),z2(z2){}

  long double getVolume(){
    return abs((this->x1 - this->x2)*(this->y1 - this->y2)*(this->z1 - this->z2));
  }

  long double volume_permutation(){
    long double volume = 0, min_r =0;

    for (int i=0; i<n;i++){
      min_r = min (ballons[permutation[i]]->closestBalloon(i, ballons), this->getNearestEdge(ballons[permutation[i]]));
      volume = volume + (4.0/3.0)*PI*powl(min_r,3.0);
      ballons[permutation[i]]->setR(min_r);
    }

    return volume;
  }

  void volumeBallon(){
    long double volume = 0;
    do{
      volume = max(volume, volume_permutation());
    }while(next_permutation(permutation,permutation+n));

    printf("%ld\n\n", lroundl(this->getVolume() - lroundl(volume)));
  }


  long double getNearestEdge(Ballon* b){
    long double min_x = min( abs( x1-(b->getX()) ), abs( x2-(b->getX()) ));
    long double min_y = min( abs( y1-(b->getY()) ), abs( y2-(b->getY()) ));
    long double min_z = min( abs( z1-(b->getZ()) ), abs( z2-(b->getZ()) ));

    return min( min(min_x, min_y), min_z);
  }
};




int main() {
  scanf("%d\n", &n);

  while (n){
    printf("Box %d: ", ++cas);

    int x1,x2,y1,y2,z1,z2;

    scanf("%d %d %d\n%d %d %d",&x1,&y1,&z1,&x2,&y2,&z2);

    Box * box = new Box(x1,x2,y1,y2,z1,z2);

    for (int i=0; i<n; i++){
      int x,y,z;
      scanf("%d %d %d\n",&x, &y,&z);
      Ballon * tmp = new Ballon(x,y,z);
      ballons[i] = tmp;
      tmp = nullptr;

      permutation[i] = i;
    }

    box->volumeBallon();

    scanf("%d\n", &n);
  }


  return 0;
}
