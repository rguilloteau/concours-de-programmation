#include <stdio.h>
#include <vector>
#include <algorithm>

using namespace std;

class Package{
   int x;
   int y;
   int weight;

public :
  Package( int x,  int y,  int w):x(x), y(y), weight(w){}
   int getX(){return x;}
   int getY(){return y;}
   int getWeight(){return weight;}

   int getDistance(Package* p){
    return abs(this->x - p->getX()) + abs(this->y - p->getY());
  }

   int getDistanceFromOrigin(){return x+y;}
};

Package* l[100000];
int direct[10000-1],byOrigin[10000-1];
int cases, capacity, n, nb_retour_base_max;

int minDist(int payload,  int index, int retour_base);

int main() {


  scanf("%d\n",&cases);

  for (int cas =0; cas<cases; cas++){
    scanf("%d", &capacity);
    scanf("%d", &n);

    for (int i=0; i<n; i++){
       int x, y, weight;
      scanf("%d %d %d", &x, &y, &weight);
      Package * tmp = new Package(x, y, weight);
      l[i]=tmp;
      tmp = nullptr;
    }

    nb_retour_base_max=0;
    int charge=0;

    for (int i=0; i<n-1; i++){
      direct[i]=l[i]->getDistance(l[i+1]);
      byOrigin[i]= l[i]->getDistanceFromOrigin()+l[i+1]->getDistanceFromOrigin();
      int w= l[i]->getWeight();
      if (charge+w>capacity){
        nb_retour_base_max++;
        charge=w;
      }
      else
        charge = charge+w;
    }

    int w= l[n-1]->getWeight();
    if (charge+w>capacity){
      nb_retour_base_max++;
      charge=w;
    }


    int answer = l[0]->getDistanceFromOrigin()+minDist(0,0,0);
    printf("%d\n", answer);
    if (cas<cases-1)
      printf("\n");


  }

  return 0;
}

int minDist(int payload,  int index, int retour_base){

  if (payload + l[index]->getWeight()>capacity)
    return -1;

  if (index==n-1)
    return l[n-1]->getDistanceFromOrigin();

  if (retour_base > nb_retour_base_max)
    return -1;


  int a = minDist(payload + l[index]->getWeight(), index+1, retour_base);
  int b = minDist(0, index+1,retour_base+1);
  int dist_b = b+byOrigin[index];

  if (a == -1)
    return dist_b;
  else if (b == -1)
    return a+direct[index];
  else
    return min (a+direct[index], dist_b);

}
