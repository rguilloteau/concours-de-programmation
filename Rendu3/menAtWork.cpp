#include <stdio.h>
#include <vector>
#include <queue>

using namespace std;

class Position{
public:
  int i;
  int j;
  int t;

  Position(int i, int j, int t):i(i), j(j), t(t){}

};

bool roads[50][50], already_seen[50][50];
int roads_period[50][50];
int last_time_seen[50][50];
//PPCM des périodes * nb max de cases par ligne
const int DELAY = 2520*50;
int delay_compt;

void clear (queue<Position *> file){
  while(!file.empty()){
    file.pop();
  }
}


bool road_open(int i,int j,int n,int t){
  if (t<=last_time_seen[i][j])
    return false;
  last_time_seen[i][j] = t;
  int p = roads_period[i][j];
  if (p==0)
    return roads[i][j];
  else{
    bool s = roads[i][j] ^ (t%(2*roads_period[i][j])>=roads_period[i][j]);
    return s;
  }
}


int next_step(int n, queue<Position *> file){

  while(1){

  if(file.empty())
    return -1;

  Position * p = file.front();
  file.pop();

  if (!already_seen[p->i][p->j]){
    already_seen[p->i][p->j]=true;
    delay_compt = 0;
  }
  else if (delay_compt>=DELAY)
    return -1;
  else
    delay_compt++;

  if (p->i==n-1 && p->j==n-1)
    return p->t;

  if ((p->i>0) && road_open(p->i-1,p->j,n,p->t+1)){
    file.push(new Position(p->i-1,p->j,p->t+1));

  }

  if ((p->i<n-1) && road_open(p->i+1,p->j,n,p->t+1)){
    file.push(new Position(p->i+1,p->j,p->t+1));
  }

  if ((p->j>0) && road_open(p->i,p->j-1,n,p->t+1)){
    file.push(new Position(p->i,p->j-1,p->t+1));
  }

  if ((p->j<(n-1)) && road_open(p->i,p->j+1,n,p->t+1)){
    file.push(new Position(p->i,p->j+1,p->t+1));
  }

  if (road_open(p->i,p->j,n,p->t+1)){
    file.push(new Position(p->i,p->j,p->t+1));
  }
}

}

int main(){

  int n,res;
  char c;

  bool line = false;

  queue<Position *> file;

  for (int i=0; i<50; i++){
    for (int j=0; j<50; j++){
      roads[i][j] = false;
      roads_period[i][j] = 0;
      last_time_seen[i][j] = -1;
    }
  }

  while (scanf("%d\n", &n)>0){

    if (line)
      printf("\n");
    else
      line =true;

    for (int i=0; i<50; i++){
      for (int j=0; j<50; j++){
        last_time_seen[i][j] = -1;
      }
    }

    for (int i=0; i<n; i++){
      for (int j=0; j<n; j++){
        scanf("%c",&c);
        roads[i][j] = (c=='.');
      }
      scanf("\n");
    }
    char d;
    for (int i=0; i<n; i++){
      for (int j=0; j<n; j++){
        scanf("%c",&d);
        d=d-48;
        roads_period[i][j]=d;
      }
      scanf("\n");
    }

    delay_compt = 0;

    Position * p = new Position(0,0,0);
    file.push(p);
    res = next_step(n, file);

    if (res == -1)
      printf("NO\n");
    else
      printf("%d\n",res);
    clear(file);

  }
}
