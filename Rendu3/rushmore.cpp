#include <stdio.h>
#include <vector>
#include <list>
#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

vector<string> traductions(26,"");
vector<bool> already_visited(26,false);
int n,m;

void cleanAlreadyVisited(){
  for (int i=0; i<26; i++)
    already_visited[i]=false;
}

void cleanTraductions(){
  for (int i=0; i<26; i++)
    traductions[i]="";
}

bool translate(char a, char b){
  if (a==b)
    return true;

  if (already_visited[a-97])
    return false;

  already_visited[a-97] = true;
  string s = traductions[a-97];
  for (int i=0; i<s.length(); i++){
    if(translate(s[i], b)){
      return true;
    }
  }

  return false;
}

bool test(string encoded, string plain) {
  if (encoded.length() != plain.length()){
    return false;
  }


  for (int i=0; i < encoded.length(); i++){
    cleanAlreadyVisited();

    if (!translate(encoded[i], plain[i])){
      return false;
    }

  }

  return true;

}

int main(){
  while(scanf("%d %d", &m, &n)==2){
    for (int i=0; i<m; i++){
      char char1;
      string char2;
      cin >> &char1 >> char2;
      traductions[char1-97] += char2;
    }

    bool res;

    for (int i = 0; i<n; i++){
      string encoded, plain;
      cin >> encoded >> plain;

      res=test(encoded, plain);

      if (res)
        printf("yes\n");
      else
        printf("no\n");
    }

    cleanTraductions();

  }


  return 0;
}
