#include <stdio.h>
#include <memory>
#include <queue>
#include <stack>

using namespace std;

const unsigned int MAXN =500;
const unsigned int MAXLEN=1001*MAXN;

unsigned int Dist[MAXN], Adj[MAXN][MAXN];
bool firstPredecessor[MAXN][MAXN];
int secondPredecessor[MAXN];
typedef pair<unsigned int, int> WeightNode; // weight goes first
priority_queue<WeightNode, std::vector<WeightNode>,std::greater<WeightNode> > Q;

void init_Firstpredecessors(int n){
  for (int i=0; i<n; i++){
    for (int j=0; j<n; j++)
      firstPredecessor[i][j] = false;
  }
}

void initAdj(int n){
  for (int i=0; i<n; i++){
    for (int j=0; j<n; j++)
      Adj[i][j]=MAXLEN;
  }
}

void init_Secondpredecessors(int n){
  for (int i=0; i < n; i++){
    Dist[i]=MAXLEN;
    secondPredecessor[i] = -1;
  }
}

void delete_edges(int d, int n){
  for (int p=0; p<n; p++){
    if (firstPredecessor[d][p]){
      Adj[p][d] = MAXLEN;
      delete_edges(p,n);
    }
  }
}

void clearQ(){
  while (!Q.empty()){
    Q.pop();
  }
}

void initDist(int n){
  for (int i=0; i<n; i++)
    Dist[i]=MAXLEN;
}

// Dijkstra pris dans le cours et adapté
void FirstDijkstra(int root, int d, int n) {
  init_Firstpredecessors(n);
  initDist(n);
  Dist[root] = 0;
  Q.push(make_pair(0, root));
  while(!Q.empty()) {
    int u = Q.top().second; // get node with least priority
    if (u==d){
      clearQ();
      return;
    }
    Q.pop();
    for (int v=0; v<n; v++) {
      unsigned int weight = Adj[u][v];
      if(Dist[v] > Dist[u] + weight) {    // shorter path found?
        Dist[v] = Dist[u] + weight;
        for (int i=0;i<n;i++)
          firstPredecessor[v][i]=false;
        firstPredecessor[v][u]=true;
        Q.push(make_pair(Dist[v], v));    // simply push, no update here
      }
      else if (Dist[v] == Dist[u] + weight)
        firstPredecessor[v][u]=true;
    }
  }
}

// Dijkstra pris dans le cours
void secondDijkstra(int root, int d, int n) {
  init_Secondpredecessors(n);
  initDist(n);
  Dist[root] = 0;
  Q.push(make_pair(0, root));
  while(!Q.empty()) {
    int u = Q.top().second; // get node with least priority
    if (u==d){
      clearQ();
      return;
    }
    Q.pop();
    for (int v=0; v<n; v++) {
      unsigned int weight = Adj[u][v];
      if(Dist[v] > Dist[u] + weight) {    // shorter path found?
        Dist[v] = Dist[u] + weight;
        secondPredecessor[v]=u;
        Q.push(make_pair(Dist[v], v));    // simply push, no update here
      }
    }
  }
}

int main(){
  unsigned int n, m,s,d,u,v,p;
  while(scanf("%d %d", &n, &m)){
    if (n==0 && m==0)
      break;

    scanf("%d %d", &s, &d);

    initAdj(n);

    for (int i=0; i<m; i++){
      scanf("%d %d %d", &u, &v, &p);
      Adj[u][v] = p;
    }


    FirstDijkstra(s,d,n);
    delete_edges(d,n);
    secondDijkstra(s,d,n);

    if (Dist[d]>=MAXLEN)
      printf("-1\n");
    else
      printf("%d\n", Dist[d]);
  }

  return 0;
}
