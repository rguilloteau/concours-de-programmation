#include <stdio.h>
#include <iostream>
#include <cmath>

using namespace std;

const double EPS = pow(10,-6);

class Point{
public:
  double x, y;
  Point() {
    x=0.0;
    y=0.0;
  }

  Point(double in_x, double in_y): x(in_x), y(in_y) {}

  Point calcul_milieu(Point a){
    Point res;
    double x1 = (a.x + x)/2;
    double y1 = (a.y + y)/2;
    res = Point(x1,y1);
    return res;
  }

  double distance(Point a){
    return pow(pow(x-a.x,2)+pow(y-a.y,2),0.5);
  }

};


class Droite{
public:
  double m, p;

  Droite(){m=0.0;p=0.0;}

  Droite(Point a, Point b){
    m = -(a.x - b.x)/(a.y-b.y);
    Point milieu = a.calcul_milieu(b);
    p = milieu.y-m*milieu.x;
  }

  Point intersection(Droite d){
    Point res;
    double m1 = m - d.m;
    double p1 = p - d.p;
    res.x = -p1/m1;
    res.y = m*res.x+p;
    return res;
  }
};

int main(){
  Point p1, p2, p3;
  while(scanf("%lf %lf\n%lf %lf\n%lf %lf\n", &p1.x, &p1.y,&p2.x, &p2.y,&p3.x, &p3.y)==6){

    Droite d1, d2;

    d1 = Droite(p1,p2);
    d2 = Droite(p2,p3);

    Point centre = d1.intersection(d2);

    double l1,l2,r;
    l1 = p1.distance(p2)/2;
    l2 = p2.distance(p3)/2;
    r  = p1.distance(centre);

    double angle1 = asin(min(1.0,l1/r));
    double angle2 = asin(min(1.0,l2/r));
    int n=0;

    for (int i=3; i<=1000; i++){
      double angle_min = M_PI/i;
      double approx1 = round(angle1/angle_min)*angle_min;
      double approx2 = round(angle2/angle_min)*angle_min;



      if (abs(angle1-approx1)<EPS && abs(angle2-approx2)<EPS){
        n=i;
        break;
      }
    }
    cout <<n<<endl;
  }

  return 0;
}
