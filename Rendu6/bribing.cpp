#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;
#define MAXN 100000

int tests[MAXN][2];
vector<double> angle(MAXN,0);

int main(){
  int n;

  while(scanf("%d\n",&n)!=EOF){
    for (int i = 0; i<n; i++){
      scanf("%d %d\n",&tests[i][0], &tests[i][1]);
    }
    for (int i = n-1; i>=0; i--){
      tests[i][0]-=tests[0][0];
      tests[i][1]-=tests[0][1];
    }


    for (int i = 1; i<n; i++){
      if (tests[i][0]>0 && tests[i][1]>=0)
        angle[i]=atan( ((double) tests[i][0])/((double)tests[i][1]) );
      else if (tests[i][0]>0 && tests[i][1]<0)
        angle[i]=atan( ((double) tests[i][0])/((double)tests[i][1]) )+2*M_PI;
      else if (tests[i][0]<0)
        angle[i]=atan( ((double) tests[i][0])/((double)tests[i][1]) )+M_PI;
      else if (tests[i][0]==0 && tests[i][1]>0)
        angle[i]=M_PI/2;
      else if (tests[i][0]==0 && tests[i][1]<0)
        angle[i]=3*M_PI/2;
      else
        angle[i]=0;
    }

    sort(angle.begin()+1, angle.begin()+n);

    double theta = angle[1];
    int meilleur = MAXN, pire = 0;
    int i_min = 1, i_max = 1;

    while(theta<=M_PI/2 && i_min<n){
      while (angle[i_min]<theta && i_min<n)
        i_min++;
      while(angle[i_max]<=theta+M_PI && i_max <n)
        i_max++;

      int count_p  =0, count_e =0;
      for(int i=i_min; i<i_max; i++){
        if (angle[i]>theta && angle[i]<M_PI+theta)
          count_p++;
        if (angle[i]==theta || angle[i]==M_PI+theta)
          count_e++;
      }
      meilleur = min(meilleur, count_p+1);
      pire = max(pire, count_p+count_e+1);
      if (i_min<n && i_max<n-1)
        theta = min(angle[i_min+1], angle[i_max]-M_PI);
      else if(i_min<n)
        theta = angle[i_min+1];
    }

    if (n==1){
      meilleur=1;
      pire =1;
    }

    cout <<meilleur <<" "<<pire<<endl;
  }
  return 0;
}
