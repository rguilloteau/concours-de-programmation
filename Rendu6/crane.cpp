#include <iostream>
#include <stdio.h>
#include <cmath>

using namespace std;

int polygon[100][2];


double aire(int n){
  double res = 0;
  for (int i=0; i<n-1; i++){
    res+=(polygon[i][0]*polygon[i+1][1]-polygon[i][1]*polygon[i+1][0]);
  }
  return 0.5*(res);
}

double find_G1_x(int n){
  double a = aire(n);
  double g1_x = 0;
  for (int i=0; i<n-1; i++){
    g1_x += (polygon[i][0]+polygon[i+1][0])*(polygon[i][0]*polygon[i+1][1]-polygon[i+1][0]*polygon[i][1]);
  }
  g1_x /=6*a;
  return g1_x;
}

bool test_stability(int xmin, int xmax, double g1){
  bool stable;
  //Test right side
  stable = !(g1-xmax>0 && xmax<=0);
  //Test left side
  stable&= !(g1-xmin<0 && xmin>=0);

  return stable;
}


int main() {
  for (int i=0; i<100; i++){
    polygon[i][0]=0;
    polygon[i][1]=0;
  }

  int n, xmin=2000, xmax=-2000;

  while(scanf("%d\n", &n)==1){
    xmin=2000;
    xmax=-2000;
    for (int i=0; i<n; i++){
      scanf("%d %d\n", &polygon[i][0], &polygon[i][1]);
      if (polygon[i][1]==0){
        xmin=min(xmin, polygon[i][0]);
        xmax=max(xmax, polygon[i][0]);
      }
    }

    xmin-=polygon[0][0];
    xmax-=polygon[0][0];

    for (int i=n-1; i>=0; i--){
      polygon[i][0]-=polygon[0][0];
      polygon[i][1]-=polygon[0][1];
    }

    double a = abs(aire(n));
    double g1 = find_G1_x(n);

    bool stable = test_stability(xmin, xmax, g1);
    if (!stable){
      cout <<"unstable"<<endl;
      continue;
    }

    double p_min = max(0.0, a*(g1-xmin)/(xmin));
    double p_max = max(0.0, a*(g1-xmax)/(xmax));
    if (p_min>p_max){
      double tmp = p_max;
      p_max = p_min;
      p_min = tmp;
    }
    if (xmin <= 0 && 0 <= xmax){
      cout <<(int) floor(p_min)<<" .. inf"<<endl;
    }
    else{
      cout <<(int) floor(p_min)<<" .. "<<(int) ceil(p_max)<<endl;
    }

  }

  return 0;
}
