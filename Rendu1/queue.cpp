#include <stdio.h>
#include <list>


int main(){

  int p, c, cas;
  cas=1;

  std::list<int> l;

  char command_type;
  while(scanf("%d", &p)==1){
    if (p!=0)
      printf("Case %d:\n", cas);
    scanf("%d", &c);

    int max = std::min(p, 1000);

    for (int i=1; i<=max; i++){
      l.push_back(i);
    }


    for (int i=0; i<c; i++){
      scanf("%s",&command_type);
      if(command_type=='E'){
        int urgence;
        scanf("%d",&urgence);
        l.remove(urgence);
        l.push_front(urgence);

      }

      else if(command_type=='N'){
        int f = l.front();
        l.pop_front();
        if (p<1000)
          l.push_back(f);
        printf("%d\n", f);
      }



    }

    cas++;
    l.clear();

  }

}
