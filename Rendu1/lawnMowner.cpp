#include <stdio.h>
#include <list>

char calcul(int n,float w,float largeur);

int main() {

  int nx, ny;
  float w;

  while(scanf("%d",&nx)==1){
    scanf("%d",&ny);
    scanf("%f", &w);

    if (nx==0)
      break;

    char test1 = calcul(nx,w,75);
    char test2 = calcul(ny,w,100);
    if ( test1 & test2 ){
      printf("YES\n");
    }
    else
      printf("NO\n");

  }

  return 0;
}

char calcul(int n,float w,float size){

  std::list<float> l (n,0);
  char test=1;

  for (auto& it : l){
    scanf("%f", &it);
  }
  l.sort();
  float max = -w/2;

  for (auto it : l){
    if(it>max+w){
      test=0;
      break;
    }
    max=it;
  }
  test = test & (max>=(size-(w/2)));
  return test;
}
