#include <stdio.h>
#include <cmath>
#include <math.h>
#include <utility>
#include <vector>
#include <algorithm>

const double SIZE = 100;
const double PI = acos(-1);

double calotte_spherique(double h, double r){
  return PI*pow(h,2.0)*(3.0*r-h)/3.0;
}

double calcul_gauche(double a, double b, std::vector<std::pair<float,float>> l){
  double volume = SIZE * SIZE * (b - a);

  for (auto trou : l){

    double z = std::get<0>(trou);
    double r = std::get<1>(trou);

    if (z + r <= a or z - r >= b) continue;

    if (z - r < a and z + r > b) volume -= (calotte_spherique(b-(z-r),r) - calotte_spherique(a-(z-r),r));

    else volume -= calotte_spherique(std::min(b,z+r) - std::max(a,z-r),r);
  }

  return volume;
}

int main() {

  int n,s;

  std::vector<std::pair<float,float>> l;

  while(scanf("%d %d", &n,&s)==2){
    double volume = pow(SIZE,3); //mm3

    int r,x,y,z;
    for (int i=0; i<n; i++){
      scanf("%d %d %d %d",&r,&x,&y,&z);

      float r_f = r/1000.0;
      float z_f = z/1000.0;
      std::pair<float,float> tmp(z_f,r_f);
      l.push_back(tmp);
      double p = 4*PI*pow((r_f),3.0)/3.0;
      volume = volume -p;
    }


    double previous=0;
    double volume_tranche = volume/s;
    double affiche;

    for (int i=0; i<s-1; i++){

      double a = 0;
      double b = SIZE;
      double c=0, slice=0;

      while (std::fabs(slice-volume_tranche)>1e-10){
        c = (a+b)/2;
        slice = calcul_gauche(previous,c,l);
        if (slice<volume_tranche)
          a=c;
        else
          b=c;
      }

      affiche = (c-previous);
      printf("%lf\n", affiche);
      previous = c;

    }
    affiche = (SIZE-previous);
    printf("%lf\n", affiche);
    l.clear();

  }
  return 0;
}
