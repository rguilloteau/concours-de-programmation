#include <stdio.h>
#include <stack>
#include <memory>
#include <list>

using namespace std;

int adj[51][51];
bool already_seen[1000];
stack<int> circuit;
int previous, start;
int nb_perles;

void init(){
  for (int i=0; i<1000; i++)
    already_seen[i]=false;


  for (int i=0; i<51; i++){
    for (int j=0; j<51; j++)
      adj[i][j] = 0;
  }
}

bool possible(){
  for (int i=1; i<51; i++){
    if (adj[0][i]%2==1)
      return false;
  }
  return true;
}


void find_circuit(int m){
  for (int i = 1; i <= 50; i++)
    if (adj[m][i] > 0 /*&& adj[0][i]>1*/){
      --adj[m][i];
      --adj[i][m];
      find_circuit(i);
      circuit.push(i);
      --nb_perles;
    }
}

void print_solution(){
  while(!circuit.empty()){
    int color = circuit.top();
    if(!previous){
      previous = color;
      start = color;
    }
    else{
      printf("%d %d\n", previous, color);
      previous = color;
    }
    circuit.pop();
  }
  printf("%d %d\n", previous, start);
}


int main(){

  int T, N;
  scanf("%d\n", &T);
  for (int t=1; t<=T; t++){

    printf("Case #%d\n", t);

    scanf("%d\n", &N);

    int x,y;
    init();

    for(int i=0; i<N; i++){
      scanf("%d %d\n",&x, &y);
        adj[x][y]++;
        adj[y][x]++;
        adj[0][x]++;
        adj[0][y]++;
        start = x;
    }
    nb_perles = N;
    previous = 0;

    if (!possible()){
      printf("some beads may be lost\n");
      continue;
    }
    find_circuit(start);

    if(nb_perles==0)
      print_solution();
    else
      printf("some beads may be lost\n");

    if (t<T){
      printf("\n");
    }



  }
}
