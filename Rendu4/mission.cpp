#include <stdio.h>
#include <algorithm>
#include <stack>
#include <memory>

using namespace std;

const int MAX = 100;
long long int m[MAX][MAX], mfinal[MAX][MAX];
int piles_rows[MAX], piles_columns[MAX];

bool DFS(int G[MAX+2][MAX+2], int s,int t,int Predecessor[2*MAX+2]){
  if (s==t)
    return true;

  for (int i=0;i<MAX;i++){
    if (G[s][i]!=0){
      Predecessor[i] = s;
      if (DFS(G, i, t, Predecessor))
        return true;
    }
  }
  return false;
}

void FordFulkerson(int G[MAX+2][MAX+2],int s,int t) {
  int GRes[MAX+2][MAX+2];// residual graph
  copy_n((int*)G, (MAX+2)*(MAX+2), (int*)GRes);// copy original graph
  int Predecessor[MAX];
  int Maxflow =0;
  for (int i=0; i<MAX; i++){
    piles_rows[i]= -1;
    piles_columns[i] = -1;
  }
  while(DFS(GRes, s, t, Predecessor)) {// find residual path
    int Bottleneck = Maxflow;// get minimal flow of residual path
    for(int v = t; v != s; v = Predecessor[v])
      Bottleneck = min(Bottleneck, GRes[Predecessor[v]][v]);
    for(int v = t; v != s; v = Predecessor[v]) {// decrease capacity along residual path
      GRes[Predecessor[v]][v] -= Bottleneck;
      GRes[v][Predecessor[v]] += Bottleneck;
      if (v!=s && Predecessor[v]!=t){
        piles_rows[Predecessor[v]]=v;
        piles_columns[v]=Predecessor[v];
      }
    }
    Maxflow += Bottleneck;
  }
}

void bipart(int height, int r, int c, long long int max_rows[], long long int max_columns[]){
  int G[MAX+2][MAX+2];
  for (int i=0; i<=MAX; i++){
    for (int j=0; j<=MAX; j++){
      G[i][j]=0;
    }
  }

  for (int i=0; i<r; i++){
    if (max_rows[i]==height){
      G[MAX][i]=1;
      for (int j=0; j<c; j++){
        if (max_columns[j]==height && m[i][j]>0 && m[i][j]<=height){
          G[i][j]=1;
        }
      }
    }
  }

  for (int j=0; j<c; j++){
    if (max_columns[j]==height)
      G[j][MAX+1]=1;
  }


  FordFulkerson(G,MAX,MAX+1);

  for (int i=0; i<r; i++){
    if (max_rows[i]==height){
      if (piles_rows[i]!= -1)
        mfinal[i][piles_rows[i]] = height;
      else{
        for (int j = 0; j<c; j++){
          if (m[i][j]==height){
            mfinal[i][j] = height;
            break;
          }
        }
      }
    }
  }
}

int main(){
  int r,c;
  long long int max_height, max_rows[MAX], max_columns[MAX];
  while(scanf("%d %d\n",&r,&c)!=EOF){
    for (int i=0; i<MAX; i++){
      for (int j=0; j<MAX; j++){
        m[i][j]=0;
        mfinal[i][j]=0;
        max_rows[i] = 0;
        max_columns[i] = 0;
      }
    }
    max_height = 0;
    for(int i=0; i<r; i++)
    {
      for (int i=0; i<MAX; i++){
        max_rows[i] = 0;
        max_columns[i] = 0;
      }
      for (int j=0; j<c; j++){
        scanf("%lld", &m[i][j]);
        max_height = max(max_height, m[i][j]);
        max_rows[i] = max(max_rows[i], m[i][j]);
        max_columns[j]=max(max_columns[j], m[i][j]);
        if (m[i][j]>0)
          mfinal[i][j]=1;
        else
          mfinal[i][j]=0;
      }
      scanf("\n");
    }

    for (long long int i=max_height; i>0; i--){
      bipart(i,r, c, max_rows, max_columns);
    }

    long long int res = 0;

    for (int i=0; i<r; i++){
      for (int j=0; j<c; j++){
        res+=m[i][j]-mfinal[i][j];
      }
    }
    printf("%lld\n",res);
  }
}
