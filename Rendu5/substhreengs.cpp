#include <stdio.h>
#include <iostream>
#include <cstring>
using namespace std;

long modulo[3]={1,0,0};

int main(){
  string s;
  while(getline(cin, s)){
    long n = s.size();
    modulo[0]=1;
    modulo[1]=0;
    modulo[2]=0;
    long res = 0, somme=0;
    for (long i=0; i<n; i++){
      char value = s[i]-48;
      if (value >=0 && value <=9){
        somme=(somme+value)%3;
        res+=modulo[somme];
        modulo[somme]+=1;
        }
      else{
        modulo[0]=1;
        modulo[1]=0;
        modulo[2]=0;
        somme=0;
      }
    }
    cout << res<<endl;

  }
}
