#include <stdio.h>
#include <queue>
#include <cstdint>
#include <memory>
#include <string>
#include <iostream>

using namespace std;

//From STM32L4XX.H
#define SET_BIT(REG, BIT)     ((REG) |= (1<<(BIT)))
#define CLEAR_BIT(REG, BIT)   ((REG) &= ~(1<<(BIT)))
#define READ_BIT(REG, BIT)    ((REG) & (1<<(BIT)))


uint32_t adj[21];
queue<pair<uint32_t,string>> file;

void clear_file(){
  while(!file.empty())
    file.pop();
}

uint32_t next_dispotion(uint32_t disposition, int n){
  uint32_t next = 0;
  for (int i=0; i<n; i++){
    if (READ_BIT(disposition, i)){
      next |= adj[i];
    }
  }
  return next;
}

string find_solution(int n){
  while(!file.empty()){
    pair<uint32_t, string> l = file.front();
    file.pop();
    if (l.first==0)
      return l.second;

    for (int i=0; i<n; i++){
      uint32_t tmp = l.first;
      CLEAR_BIT(tmp, i);
      tmp = next_dispotion(tmp, n);
      file.emplace(tmp, l.second+" "+to_string(i));
    }
  }
  return "";
}

int main(){
  int n=1,m=1;
  int cas = 1;
  while(scanf("%d %d\n", &n, &m)!=EOF && (n!=0) &&(m!=0)){

    for (int i=0; i<21; i++){
      adj[i]=0;
    }

    int x,y;
    for (int i=0; i<m; i++){
      scanf("%d %d\n", &x, &y);
      SET_BIT(adj[x],y);
      SET_BIT(adj[y],x);
    }
    if (m>=n){
      printf("Impossible\n");
      continue;
    }
    else{
      clear_file();
      uint32_t disposition = (1<<21)-1 ;
      file.emplace(disposition, "");

      string res = find_solution(n);
      if (res=="")
        cout <<"Impossible"<<endl;
      else{
        cout <<res.length()/2<<":"<<res<<endl;
        cas++;
      }
      scanf("\n");
    }
  }
}
