#include <stdio.h>
#include <cstring>
#include <iostream>
#include <cmath>
#include <memory>
#include <algorithm>
#include <vector>
using namespace std;

class Triple{
public:
  string str;
  int t;
  int pos;
  Triple(string str, int t, int pos):str(str), t(t), pos(pos){}
};

const unsigned int MAXN = pow(10,6);
int T[MAXN+1];
vector<Triple *> sequences(10,new Triple("",0,0));

bool pairCompare(Triple* p1, Triple* p2){
  if (p1->t != p2->t){
    return p1->t < p2->t;
  }
  else{
    return p1->pos < p2->pos;
  }
}

//Adaptation de l'algorithme de Knuth–Morris–Pratt vu en cours
void knuth(int n, Triple* seq){
  T[0]=-1;
  string p=seq->str;
  int np = p.size();
  int cnd = 0;
  for (int i=1; i<=np; i++){
    T[i]=cnd;
    while (cnd >=0 && p[cnd] != p[i])
      cnd = T[cnd];
    cnd++;
  }
  seq->t = max(0, n-(2*np-T[np]));
}

int main() {
  for (int i=0; i<10; i++)
    sequences[i]=new Triple("",0,0);

  int n,s;
  while(scanf("%d %d", &n, &s)!=EOF){
    for (int i=0; i<s; i++){
      cin>>sequences[i]->str;
      sequences[i]->t=0;
      sequences[i]->pos=i;
      knuth(n, sequences[i]);
    }
    sort(sequences.begin(), sequences.begin()+s, pairCompare);
    for (int i=0; i<s; i++){
      cout << sequences[i]->str;
      if (i<s-1){
        cout<<endl;
      }
    }

  }
  return 0;
}
